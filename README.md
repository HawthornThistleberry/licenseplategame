# License Plate Game

This is an exercise in learning React. It's a simple solver for the license plate game.

What is the license plate game, I hear you ask? Surely you've been on a long drive with an overly energetic child, or an overly energetic adult who still loves puzzle-thinking. And yet you still don't know? Well, I'll tell you.

Look at any license plate that has the usual three letters. (Offer not available in states that no longer do their license plates that way.) Think of a word that contains those same letters, in that same order, but not necessarily contiguous.

That's it. Except of course some words are better than others. Some letter combinations are very hard (try thinking of a word for XGJ) and some very easy (RCT = react) but most are somewhere in between. When it's easy to find one or several, the passengers will judge which is the best based on criteria like these:

* use the usual Scrabble rules: proper nouns don't count, for instance
* shorter words are usually better than long ones because they're harder to find
* words whose first letter is not the first letter on the plate are worth extra
* words that are particularly unusual, apropos to the moment, or just cool are worth more

With hard combinations, compound words may be a temptation. The question here is, are you finding a real word, that's in the dictionary?

* `butterfly` may be etmyologically a compound word, but it is absolutely a word in its own right now, and it counts full
* `undercarriage` may be in the dictionary so technically it counts, but it feels forced so it's not as good a solution
* `apeman` may be gramatically acceptable, but since `ape-man` is the more common spelling, this is an option of last resort, and loses to anything else
* `firetruck` is not really a word, as much as you might want it to be, so it doesn't count

Yes, these rules are subjective. This isn't the kind of game you keep score on. It's an idle diversion.

So what's the React program? Well, when you're stumped, this uses a dictionary word list to give a mostly-definitive list of answers. You can answer the question, "was there a better option than I came up with?" or "what's the answer I couldn't think of?" or even "was there even really an answer?"

## Credits

Much of this is derived from the lesson "React: Getting Started" on Pluralsight by Samer Buna, and by the code he wrote during that lesson for the Star Match game. (I had gotten to that point when I felt like I couldn't go further into his lessons without having done some of it myself.)

## Trying It

Browse to https://license-plate-solver.glitch.me/ and give it a try.

Or make your own playground at https://jscomplete.com/playground. Post the CSS into the CSS pane, and the JS into the main pane, and run. Tada.

**Note**: The word list I have loading does not include valid words that are conjugations of existing words in many cases (e.g., simple past tenses) so its list of solutions is limited thereby. Finding a more exhaustive word list is something I will have to do if I ever put this anywhere 'real' but that doesn't affect the programming challenge so I am not worrying about that right now.

The next chapter is about hosting it outside the playground, and now I have something to host there!
