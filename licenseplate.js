///////////////////////////////////////////////////////////////////////////////
// License Plate Solver by Frank J. Perricone <hawthorn@foobox.com>
//
// https://jscomplete.com/playground/s457154

///////////////////////////////////////////////////////////////////////////////
// Constants

const emspace = ' ';
// note that the above isn't a 0x20 space, it's an em quad space, U+2000, to ensure CSS alignment
const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

///////////////////////////////////////////////////////////////////////////////
// Word List - a complete dictionary and functions to search it by regex

var wordList = [];

var request = new XMLHttpRequest();
request.open('GET', 'https://raw.githubusercontent.com/dwyl/english-words/master/words.txt', true);
request.send(null);
request.onreadystatechange = function () {
	if (request.readyState === 4 && request.status === 200) {
		var type = request.getResponseHeader('Content-Type');
		if (type.indexOf("text") !== 1) {
			wordList = request.responseText.split('\n');
		}
	}
}

// do a search through the word list for words to fit
var matchingWords = (letters) => {
	var regex = '.*';
	letters.map(l => {regex += l + '.*'});
	return wordList.filter(word => word.toUpperCase().match(regex));
}

///////////////////////////////////////////////////////////////////////////////
// SelectedLetter component: your license plate letter selections
const SelectedLetter = props => (
	<button 
      className="letter" 
      onClick={() => props.onClick(props.id)}
      style={{backgroundColor: (props.highlight ? 'lightgreen' : 'lightgray')}}    
    >
		{props.letter}
	</button>
);

///////////////////////////////////////////////////////////////////////////////
// Picker component: a letter selector button which might be invisible
const Picker = props => (
	<button
		className="pickerbutton"
		onClick={() => props.onClick(props.label)}
		style={{visibility: (props.visible ? '' : 'hidden')}}
	>
		{props.label}
	</button>
);

///////////////////////////////////////////////////////////////////////////////
// CancelButton component: a big cancel button that looks and works mostly like the letters
const CancelButton = props => (
	<button
		className="cancelbutton"
		onClick={() => props.onClick(props.id)}
		style={{visibility: (props.visible ? '' : 'hidden')}}
	>
		{props.label}
	</button>
);

///////////////////////////////////////////////////////////////////////////////
// ResultsList component: just a list of word matches
const ResultsList = props => (
	<textarea
		rows='13'
		disabled
		readonly
		className="results"
    value={props.results.join('\n')}
	>
	</textarea>
);

///////////////////////////////////////////////////////////////////////////////
// ResultsCount component: just a count
const ResultsCount = props => (
	<div className="count">Matches Found: {props.count}</div>
);

///////////////////////////////////////////////////////////////////////////////
// The main game state component
const useGameState = lettersOnPlate => {
	const [selected, setSelected] = useState(0);
	const [letters, setLetters] = useState(utils.range(1,lettersOnPlate).map(n => emspace));
	const [results, setResults] = useState([]);

	const setLetter = (position, newLetter) => {
		var newLetters = letters;
		newLetters[position - 1] = newLetter;
		setLetters(newLetters);
		// catch if all three letters are set and if so fire off results search
		if (newLetters.filter(s => s == emspace).length == 0) {
			//setResults(['you','have','selected','letters!']);
			setResults(matchingWords(letters));
		}
	}

	const setSelection = (newSelection) => {
		if (selected == newSelection) return;
		setSelected(newSelection);
	}

	return { letters, selected, results, setSelection, setLetter };
}

///////////////////////////////////////////////////////////////////////////////
// The primary component
const LicensePlateSolver = () => {
	const lettersOnPlate = 3;
	const {
		letters,
		selected,
		results,
		setSelection,
		setLetter
	} = useGameState(lettersOnPlate);

	// on selecting a license plate letter, highlight that letter and show the picker grid
	const onLetterClick = (id) => {
		setSelection(id);
	}
  
  	// on selecting the cancel button, dehighlight and then hide the picker grid
	const onCancelClick = (id) => {
		setSelection(0);
	}
  
  	// on selecting a letter in the picker grid, set that letter and then do as cancel
	const onPickerClick = (letter) => {
		if (selected != 0) {
			// this should always happen but let's be sure and check anyway
			setLetter(selected, letter);
		}
		setSelection(0);
	}
  
  	// the JSX of the page itself
	return (
		<div className="solver">
			{/* a heading that shows the instructions */}
			<div className="help">
				Choose the letters on a license plate to see the <a href='https://bitbucket.org/HawthornThistleberry/licenseplategame/' target='_blank'>possible matches</a>.			</div>
			<div className="body">
				<div className="left">
					{/* the three license plate letters */}
					{utils.range(1, lettersOnPlate).map(letterID => 
						<SelectedLetter
							key={letterID} 
							id={letterID}
							letter={letters[letterID-1]}
							highlight={selected == letterID ? true : false}
							onClick={onLetterClick}
						/>
					)}
					{/*  the picker grid, which will start hidden */}
					<div className="picker">
						{/*  letter buttons */}
						{alphabet.map(ch => 
							<Picker
								key={ch}
								label={ch}
								visible={selected == 0 ? false : true}
								onClick={onPickerClick}
							/>
						)}
						{/*  the cancel button */}
						<CancelButton
							key={'Cancel'}
							label={'Cancel'}
							visible={selected == 0 ? false : true}
							onClick={onCancelClick}
						/>
					</div>
				</div>
				{/*  just results show here */}
				<div className="right">
					<ResultsList key={'Results'} results={results} />
				</div>
			</div>
			{/*  at the bottom, a count of results */}
			<ResultsCount key={'Count'} count={results.length} />
		</div>
	);
};

const utils = {
  // create an array of numbers between min and max (edges included)
  range: (min, max) => Array.from({ length: max - min + 1 }, (_, i) => min + i),
};

// and now that we have it all, make it live!
ReactDOM.render(<LicensePlateSolver />, mountNode);
